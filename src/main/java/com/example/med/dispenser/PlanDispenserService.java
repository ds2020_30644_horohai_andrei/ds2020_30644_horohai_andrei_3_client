package com.example.med.dispenser;

import java.rmi.Remote;
import java.util.List;

public interface PlanDispenserService extends Remote {

    List<PlanDispenser> getPlanDispenserList(int pacientId);
    void sendMessage(String message);


}
