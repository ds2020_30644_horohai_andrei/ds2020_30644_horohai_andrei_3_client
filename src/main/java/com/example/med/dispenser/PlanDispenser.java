package com.example.med.dispenser;

import java.io.Serializable;

public class PlanDispenser implements Serializable {

    private long id;
    private int pacientId;
    private String medicament;
    private String dosage;
    private String period;
    // private String breakfast;
    //private String noon;
    // private String evening;


    public PlanDispenser(){

    }

    public PlanDispenser(long id, String medicament , String dosage , String period){
        this.id = id;
        this.medicament = medicament;
        this.dosage = dosage;
        this.period = period;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPacientId() {
        return pacientId;
    }

    public void setPacientId(int pacientId) {
        this.pacientId = pacientId;
    }

    public String getMedicament() {
        return medicament;
    }

    public void setMedicament(String medicament) {
        this.medicament = medicament;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    @Override
    public String toString() {
        return "PlanDispenser{" +
                "id=" + id +
                ", medicament='" + medicament + '\'' +
                ", dosage='" + dosage + '\'' +
                ", period='" + period + '\'' +
                '}';
    }
}


