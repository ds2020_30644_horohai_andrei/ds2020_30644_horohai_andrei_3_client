package com.example.med;

import com.example.med.GUI.UserInterface;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;
import com.example.med.dispenser.PlanDispenser;
import com.example.med.dispenser.PlanDispenserService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@SpringBootApplication
@Configuration
public class MedApplication extends JPanel{







	@Bean
	public HttpInvokerProxyFactoryBean invoker(){
		HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
		invoker.setServiceUrl("https://andrei-horohai-30644-backend-2.herokuapp.com/dispenser");
		invoker.setServiceInterface(PlanDispenserService.class);
		return invoker;
	}


	public static void main(String[] args) throws  Exception{
		PlanDispenserService service = SpringApplication.run(MedApplication.class, args)
				.getBean(PlanDispenserService.class);
		System.setProperty("java.awt.headless","false");
		List<PlanDispenser> plans = new ArrayList<PlanDispenser>();
		plans = service.getPlanDispenserList(2);
		///System.out.println(plans.get(2).getMedicament());

		//new UserInterface(plans);
		JFrame frame = new JFrame("Pill Dispenser");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.add(new UserInterface(plans , service));
		//frame


		frame.setLocationByPlatform( true );
		frame.pack();
		frame.setVisible(true);




	}



}
