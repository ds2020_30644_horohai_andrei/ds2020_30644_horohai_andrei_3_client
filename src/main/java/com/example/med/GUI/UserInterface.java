package com.example.med.GUI;

import com.example.med.MedApplication;
import com.example.med.dispenser.PlanDispenser;
import com.example.med.dispenser.PlanDispenserService;
import org.springframework.boot.SpringApplication;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.HttpCookie;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

public class UserInterface extends JPanel  {





    private JTable table1;

    private JLabel label;
    private JButton Breakfast = new JButton("Breakfast");
    private JButton Noon = new JButton("Noon");
    private JButton Evening = new JButton("Evening");

    JScrollPane pane;
    DefaultTableModel model = new DefaultTableModel();

    Object[] columns = {"id", "medicament","dosage","period","takenBreakfast", "takenNoon", "takenEvening"};

    //breakfast intre 8 si 13 ; Noon intre 13 si 18 ; evening intre 18 si 23

    public UserInterface(List<PlanDispenser> planDispenserList, PlanDispenserService service) throws RemoteException {


        table1 = new JTable();
        pane = new JScrollPane(table1);
        add(pane);
        pane.setBounds(300,10,200,250);


        String[][] objects = new String[planDispenserList.size()][];
        int i = 0;
        for (PlanDispenser planDispenser : planDispenserList) {
            objects[i] = new String[10];

            objects[i][0] = String.valueOf(planDispenser.getId());
            objects[i][1] = planDispenser.getMedicament().toString();
            objects[i][2] = planDispenser.getDosage().toString();
            objects[i][3] = planDispenser.getPeriod().toString();
            if (planDispenser.getDosage().toString().charAt(0) == '1') {
                objects[i][4] = "Pill to take";
            } else objects[i][4] = "-";
            if (planDispenser.getDosage().toString().charAt(2) == '1') {
                objects[i][5] = "Pill to take";
            } else objects[i][5] = "-";
            if (planDispenser.getDosage().toString().charAt(4) == '1') {
                objects[i][6] = "Pill to take";
            } else objects[i][6] = "-";
            i++;

        }
        model.setDataVector(objects, columns);
        table1.setModel(model);




        add(Breakfast);
        add(Noon);
        add(Evening);

        Breakfast.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                    String s;
                    char b;

                    s = model.getValueAt(table1.getSelectedRow(),2).toString();
                    if( s.charAt(0) == '1') {
                        model.setValueAt("Taken", table1.getSelectedRow(), 4);
                        service.sendMessage("pacientul a luat medicamentul ---> " + model.getValueAt(table1.getSelectedRow(),1).toString() + " <--- la Breakfast" );

                    }
                }

        });


        Noon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String s;
                char b;

                s = model.getValueAt(table1.getSelectedRow(),2).toString();
                if( s.charAt(2) == '1') {
                    model.setValueAt("Taken", table1.getSelectedRow(), 5);
                    service.sendMessage("pacientul a luat medicamentul ---> " + model.getValueAt(table1.getSelectedRow(),1).toString() + " <--- la Noon" );

                }
            }

        });

        Evening.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Calendar now = Calendar.getInstance();
                String s;
                char b;

                s = model.getValueAt(table1.getSelectedRow(),2).toString();
                if( s.charAt(4) == '1' && now.get(Calendar.HOUR_OF_DAY) < 18) {
                    model.setValueAt("Taken", table1.getSelectedRow(), 6);
                    service.sendMessage("pacientul a luat medicamentul ---> " + model.getValueAt(table1.getSelectedRow(),1).toString() + " <--- la Evening" );

                }

                if(now.get(Calendar.HOUR_OF_DAY)<18)
                    service.sendMessage("pacientul a vrut sa ia --> " + model.getValueAt(table1.getSelectedRow(),1).toString() + "<--- mai repede" );

            }

        });

        final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        int interval = 10000; // 1000 ms

        new Timer(interval, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Calendar now = Calendar.getInstance();
               // System.out.println(dateFormat.format(now.getTime()));
                for(int m =0 ; m<10 ; m++) {
                   if(model.getValueAt(m,4).toString().equals("Pill to take") && now.get(Calendar.HOUR_OF_DAY)>13)
                        service.sendMessage("pacientul nu a luat --->" + model.getValueAt(m,1).toString()+"<----la Breakfast");
                   if(model.getValueAt(m,5).toString().equals("Pill to take") && now.get(Calendar.HOUR_OF_DAY)>18)
                        service.sendMessage("pacientul nu a luat --->" + model.getValueAt(m,1).toString()+"<----la Noon");
                   if(model.getValueAt(m,6).toString().equals("Pill to take") && now.get(Calendar.HOUR_OF_DAY)>22)
                        service.sendMessage("pacientul nu a luat --->" + model.getValueAt(m,1).toString()+"<----la Evening");
                }
            }
        }).start();



     //   Thread.currentThread().join();












    }


}
